﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iActivateSyncTool.Data.Interfaces
{
    public interface IDbEntity
    {
        long ApiId { get; set; }
    }

    public interface IDbCampaign : IDbEntity
    {
        string Name { get; set; }
    }
}
