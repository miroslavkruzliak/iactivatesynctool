﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iActivateSyncTool.Data.ApiEntities
{
    public class ApiCampaign : IApiEntity
    {
        public long ApiId { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
    }
}
