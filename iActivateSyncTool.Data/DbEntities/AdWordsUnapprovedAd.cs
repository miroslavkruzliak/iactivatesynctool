﻿using System;
using System.Collections.Generic;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class AdWordsUnapprovedAd
    {
        public long Id { get; set; }
        public long AdGroupId { get; set; }
        public long CampaignId { get; set; }
        public int AccountId { get; set; }
        public string Headline { get; set; }
        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public string VisibleUrl { get; set; }
        public string DestinationUrl { get; set; }
        public int? TemplateId { get; set; }
        public string Errors { get; set; }
        public DateTime Created { get; set; }
        public bool IsAdWordsError { get; set; }
        public long? DevicePreference { get; set; }
        public string TrackingUrlTemplate { get; set; }
        public string Path1 { get; set; }
        public string Path2 { get; set; }
        public string MobileUrl { get; set; }
    }
}
