﻿using System;
using System.Collections.Generic;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class BingAdGroup
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long CampaignId { get; set; }
        public int AccountId { get; set; }
        public string AdGroupType { get; set; }
        public long? Cpc { get; set; }
        public int? TemplateId { get; set; }
        public long BingId { get; set; }
        public DateTime Modified { get; set; }
        public bool IsDisabled { get; set; }
        public bool IsPaused { get; set; }

        public virtual BingCampaign Campaign { get; set; }
    }
}
