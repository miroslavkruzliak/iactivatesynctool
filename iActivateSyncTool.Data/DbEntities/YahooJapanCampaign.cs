﻿using System;
using System.Collections.Generic;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class YahooJapanCampaign
    {
        public YahooJapanCampaign()
        {
            YahooJapanAd = new HashSet<YahooJapanAd>();
            YahooJapanAdGroup = new HashSet<YahooJapanAdGroup>();
            YahooJapanAdGroupTemplate = new HashSet<YahooJapanAdGroupTemplate>();
            YahooJapanKeyword = new HashSet<YahooJapanKeyword>();
            YahooJapanUnapprovedAdGroup = new HashSet<YahooJapanUnapprovedAdGroup>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public int AccountId { get; set; }
        public long Budget { get; set; }
        public long Cpc { get; set; }
        public string CampaignType { get; set; }
        public long YahooJapanId { get; set; }

        public virtual ICollection<YahooJapanAd> YahooJapanAd { get; set; }
        public virtual ICollection<YahooJapanAdGroup> YahooJapanAdGroup { get; set; }
        public virtual ICollection<YahooJapanAdGroupTemplate> YahooJapanAdGroupTemplate { get; set; }
        public virtual ICollection<YahooJapanKeyword> YahooJapanKeyword { get; set; }
        public virtual ICollection<YahooJapanUnapprovedAdGroup> YahooJapanUnapprovedAdGroup { get; set; }
        public virtual YahooJapanAccount Account { get; set; }
    }
}
