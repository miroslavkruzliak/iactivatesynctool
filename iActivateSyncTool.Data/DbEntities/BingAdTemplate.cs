﻿using System;
using System.Collections.Generic;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class BingAdTemplate
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string Title { get; set; }
        public string DisplayUrl { get; set; }
        public string DestinationUrl { get; set; }
        public int AccountId { get; set; }
        public long? CampaignId { get; set; }
        public long? AdGroupId { get; set; }
        public int? AlternativeTo { get; set; }
        public string CampaignType { get; set; }
        public string AdGroupType { get; set; }
        public bool Active { get; set; }
        public bool IsExclusive { get; set; }
        public bool IsMobilePreferred { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public double? UtcOffset { get; set; }
        public bool UseStartEndDate { get; set; }
        public string CampaignName { get; set; }
        public string AdGroupName { get; set; }
        public string Title2 { get; set; }
        public string Path1 { get; set; }
        public string Path2 { get; set; }
        public string MobileUrl { get; set; }
        public string TrackingUrlTemplate { get; set; }
    }
}
