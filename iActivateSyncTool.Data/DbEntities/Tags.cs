﻿using System;
using System.Collections.Generic;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class Tags
    {
        public long Id { get; set; }
        public string TagName { get; set; }
        public string TagValue { get; set; }
        public int AccountId { get; set; }
        public long CampaignId { get; set; }
        public long? AdGroupId { get; set; }
    }
}
