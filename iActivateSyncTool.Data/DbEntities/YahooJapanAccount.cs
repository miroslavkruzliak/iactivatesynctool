﻿using System;
using System.Collections.Generic;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class YahooJapanAccount
    {
        public YahooJapanAccount()
        {
            YahooJapanAd = new HashSet<YahooJapanAd>();
            YahooJapanAdGroup = new HashSet<YahooJapanAdGroup>();
            YahooJapanAdGroupTemplate = new HashSet<YahooJapanAdGroupTemplate>();
            YahooJapanAdTemplate = new HashSet<YahooJapanAdTemplate>();
            YahooJapanCampaign = new HashSet<YahooJapanCampaign>();
            YahooJapanKeyword = new HashSet<YahooJapanKeyword>();
            YahooJapanKeywordTemplate = new HashSet<YahooJapanKeywordTemplate>();
            YahooJapanUnapprovedAd = new HashSet<YahooJapanUnapprovedAd>();
            YahooJapanUnapprovedAdGroup = new HashSet<YahooJapanUnapprovedAdGroup>();
            YahooJapanUnapprovedKeyword = new HashSet<YahooJapanUnapprovedKeyword>();
        }

        public int Id { get; set; }
        public long AccountId { get; set; }
        public string ApiAccountId { get; set; }
        public string ApiAccountPassword { get; set; }
        public string License { get; set; }

        public virtual ICollection<YahooJapanAd> YahooJapanAd { get; set; }
        public virtual ICollection<YahooJapanAdGroup> YahooJapanAdGroup { get; set; }
        public virtual ICollection<YahooJapanAdGroupTemplate> YahooJapanAdGroupTemplate { get; set; }
        public virtual ICollection<YahooJapanAdTemplate> YahooJapanAdTemplate { get; set; }
        public virtual ICollection<YahooJapanCampaign> YahooJapanCampaign { get; set; }
        public virtual ICollection<YahooJapanKeyword> YahooJapanKeyword { get; set; }
        public virtual ICollection<YahooJapanKeywordTemplate> YahooJapanKeywordTemplate { get; set; }
        public virtual ICollection<YahooJapanUnapprovedAd> YahooJapanUnapprovedAd { get; set; }
        public virtual ICollection<YahooJapanUnapprovedAdGroup> YahooJapanUnapprovedAdGroup { get; set; }
        public virtual ICollection<YahooJapanUnapprovedKeyword> YahooJapanUnapprovedKeyword { get; set; }
        public virtual ApiAccount IdNavigation { get; set; }
    }
}
