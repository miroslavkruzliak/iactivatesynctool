﻿using iActivateSyncTool.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class BingCampaign : IDbCampaign
    {
        public BingCampaign()
        {
            BingAdGroup = new HashSet<BingAdGroup>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public int AccountId { get; set; }
        public long Budget { get; set; }
        public long Cpc { get; set; }
        public string CountryTargeting { get; set; }
        public string LanguageTargeting { get; set; }
        public string CampaignType { get; set; }
        [Column("BingId")]
        public long ApiId { get; set; }

        public virtual ICollection<BingAdGroup> BingAdGroup { get; set; }
    }
}
