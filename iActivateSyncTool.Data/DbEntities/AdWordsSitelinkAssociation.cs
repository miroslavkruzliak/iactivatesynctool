﻿using System;
using System.Collections.Generic;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class AdWordsSitelinkAssociation
    {
        public long Id { get; set; }
        public long SitelinkItemId { get; set; }
        public int AccountId { get; set; }
        public long CampaignId { get; set; }
        public long? AdGroupId { get; set; }
    }
}
