﻿using System;
using System.Collections.Generic;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class TaskScheduler
    {
        public TaskScheduler()
        {
            TaskRuns = new HashSet<TaskRuns>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int AccountId { get; set; }
        public bool Immediately { get; set; }
        public bool Daily { get; set; }
        public DateTime? DailyAt { get; set; }
        public bool Weekly { get; set; }
        public int? WeeklyDays { get; set; }
        public DateTime? WeeklyAt { get; set; }
        public bool Monthly { get; set; }
        public int? MonthlyDays { get; set; }
        public DateTime? MonthlyAt { get; set; }
        public bool Enabled { get; set; }
        public DateTime Timestamp { get; set; }

        public virtual ICollection<TaskRuns> TaskRuns { get; set; }
    }
}
