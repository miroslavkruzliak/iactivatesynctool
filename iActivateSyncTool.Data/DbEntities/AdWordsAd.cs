﻿using System;
using System.Collections.Generic;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class AdWordsAd
    {
        public long Id { get; set; }
        public string HeadLine { get; set; }
        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public string VisibleUrl { get; set; }
        public string DestinationUrl { get; set; }
        public int AccountId { get; set; }
        public long AdGroupId { get; set; }
        public long GoogleId { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? TemplateId { get; set; }
        public string Errors { get; set; }
        public long? DevicePreference { get; set; }
        public long CampaignId { get; set; }
        public string TrackingUrlTemplate { get; set; }
        public string Path1 { get; set; }
        public string Path2 { get; set; }
        public string MobileUrl { get; set; }
    }
}
