﻿using System;
using System.Collections.Generic;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class TaskRuns
    {
        public long Id { get; set; }
        public int? TaskSchedulerId { get; set; }
        public int AccountId { get; set; }
        public DateTime RunDateTime { get; set; }
        public bool IsScheduled { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual TaskScheduler TaskScheduler { get; set; }
    }
}
