﻿using System;
using System.Collections.Generic;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class BingUnapprovedAd
    {
        public long Id { get; set; }
        public int AccountId { get; set; }
        public long CampaignId { get; set; }
        public long AdGroupId { get; set; }
        public string Text { get; set; }
        public string Title { get; set; }
        public string DisplayUrl { get; set; }
        public string DestinationUrl { get; set; }
        public long? DevicePreference { get; set; }
        public int? TemplateId { get; set; }
        public string Errors { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsBingError { get; set; }
        public string Title2 { get; set; }
        public string Path1 { get; set; }
        public string Path2 { get; set; }
        public string MobileUrl { get; set; }
        public string TrackingUrlTemplate { get; set; }
    }
}
