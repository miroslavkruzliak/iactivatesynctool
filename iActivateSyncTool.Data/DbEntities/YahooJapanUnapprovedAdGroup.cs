﻿using System;
using System.Collections.Generic;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class YahooJapanUnapprovedAdGroup
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long CampaignId { get; set; }
        public int AccountId { get; set; }
        public string Error { get; set; }
        public bool IsYahooJapanError { get; set; }

        public virtual YahooJapanAccount Account { get; set; }
        public virtual YahooJapanCampaign Campaign { get; set; }
    }
}
