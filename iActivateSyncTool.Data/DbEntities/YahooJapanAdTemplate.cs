﻿using System;
using System.Collections.Generic;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class YahooJapanAdTemplate
    {
        public int Id { get; set; }
        public string HeadLine { get; set; }
        public string Description1 { get; set; }
        public string Description2 { get; set; }
        public string DisplayUrl { get; set; }
        public string DestinationUrl { get; set; }
        public int AccountId { get; set; }
        public string CampaignName { get; set; }
        public string AdGroupName { get; set; }
        public int? AlternativeTo { get; set; }
        public string CampaignType { get; set; }
        public string AdGroupType { get; set; }
        public bool Active { get; set; }
        public bool IsExclusive { get; set; }
        public bool IsMobilePreferred { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public double? UtcOffset { get; set; }
        public bool UseStartEndDate { get; set; }
        public long? CampaignId { get; set; }
        public long? AdGroupId { get; set; }

        public virtual YahooJapanAccount Account { get; set; }
        public virtual YahooJapanAdTemplate AlternativeToNavigation { get; set; }
        public virtual ICollection<YahooJapanAdTemplate> InverseAlternativeToNavigation { get; set; }
    }
}
