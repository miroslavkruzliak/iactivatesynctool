﻿using System;
using System.Collections.Generic;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class AdWordsFeedItem
    {
        public long Id { get; set; }
        public int AccountId { get; set; }
        public long MainCampaignId { get; set; }
        public long FeedGoogleId { get; set; }
        public string Values { get; set; }
        public long GoogleId { get; set; }
        public long? GeoLocationId { get; set; }
    }
}
