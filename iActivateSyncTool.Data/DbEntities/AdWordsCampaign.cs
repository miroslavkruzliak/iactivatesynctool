﻿using iActivateSyncTool.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class AdWordsCampaign : IDbCampaign
    {
        public AdWordsCampaign()
        {
            AdWordsAdGroup = new HashSet<AdWordsAdGroup>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public int AccountId { get; set; }
        public long Budget { get; set; }
        public long Cpc { get; set; }
        public string CountryTargeting { get; set; }
        public string LanguageTargeting { get; set; }
        public string CampaignType { get; set; }
        [Column("GoogleId")]
        public long ApiId { get; set; }

        public virtual ICollection<AdWordsAdGroup> AdWordsAdGroup { get; set; }
    }
}
