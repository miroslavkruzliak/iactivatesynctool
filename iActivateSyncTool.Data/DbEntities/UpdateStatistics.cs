﻿using System;
using System.Collections.Generic;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class UpdateStatistics
    {
        public long Id { get; set; }
        public long UpdateId { get; set; }
        public string Type { get; set; }
        public double Value { get; set; }

        public virtual UpdateFeedLog Update { get; set; }
    }
}
