﻿using System;
using System.Collections.Generic;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class AdWordsAccount
    {
        public int Id { get; set; }
        public string AdwordsId { get; set; }
        public string ClientId { get; set; }
        public string AdwordsPass { get; set; }
        public long ClientCustomerId { get; set; }
        public string ClientSecret { get; set; }
        public string AdwordsToken { get; set; }

        public virtual ApiAccount IdNavigation { get; set; }
    }
}
