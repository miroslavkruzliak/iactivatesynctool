﻿using System;
using System.Collections.Generic;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class UserPermission
    {
        public int Id { get; set; }
        public int? AccountId { get; set; }
        public int UserId { get; set; }
        public string Permission { get; set; }
        public bool Value { get; set; }
        public bool Active { get; set; }

        public virtual User User { get; set; }
    }
}
