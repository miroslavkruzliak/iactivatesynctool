﻿using System;
using System.Collections.Generic;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class AdWordsSitelinkTemplate
    {
        public int Id { get; set; }
        public bool IsAdgroupSitelink { get; set; }
        public string Text { get; set; }
        public string Url { get; set; }
        public string Line2 { get; set; }
        public string Line3 { get; set; }
        public int AccountId { get; set; }
        public long? CampaignId { get; set; }
        public long? AdGroupId { get; set; }
        public int? AlternativeTo { get; set; }
        public string CampaignType { get; set; }
        public string AdGroupType { get; set; }
        public string SitelinkType { get; set; }
        public bool Active { get; set; }
        public bool IsExclusive { get; set; }
        public bool IsMobilePreferred { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public double? UtcOffset { get; set; }
        public bool UseStartEndDate { get; set; }
        public string CampaignName { get; set; }
        public string AdGroupName { get; set; }
    }
}
