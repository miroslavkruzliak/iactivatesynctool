﻿using System;
using System.Collections.Generic;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class UpdateFeedLog
    {
        public UpdateFeedLog()
        {
            UpdateStatistics = new HashSet<UpdateStatistics>();
        }

        public long Id { get; set; }
        public int AccountId { get; set; }
        public string Status { get; set; }
        public string Call { get; set; }
        public string Info { get; set; }
        public DateTime Date { get; set; }
        public int Duration { get; set; }
        public int DurationUpdating { get; set; }
        public int DurationGenerating { get; set; }
        public long? TaskRunsId { get; set; }
        public int DurationQueued { get; set; }
        public string Progress { get; set; }
        public string Error { get; set; }
        public short? Api { get; set; }

        public virtual ICollection<UpdateStatistics> UpdateStatistics { get; set; }
    }
}
