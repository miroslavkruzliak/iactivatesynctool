﻿using System;
using System.Collections.Generic;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class YahooJapanUnapprovedAd
    {
        public long Id { get; set; }
        public long AdGroupId { get; set; }
        public long CampaignId { get; set; }
        public int AccountId { get; set; }
        public string Headline { get; set; }
        public string Description1 { get; set; }
        public string Description2 { get; set; }
        public string DisplayUrl { get; set; }
        public string DestinationUrl { get; set; }
        public int? TemplateId { get; set; }
        public string Errors { get; set; }
        public DateTime Created { get; set; }
        public bool IsYahooJapanError { get; set; }
        public long? DevicePreference { get; set; }

        public virtual YahooJapanAccount Account { get; set; }
    }
}
