﻿using System;
using System.Collections.Generic;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class BingAdGroupTemplate
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int AccountId { get; set; }
        public long? CampaignId { get; set; }
        public string CampaignType { get; set; }
        public string GenerateAdGroupType { get; set; }
        public bool Active { get; set; }
        public string CreateCondition { get; set; }
        public string RemoveCondition { get; set; }
        public string CampaignName { get; set; }
        public long? MaxCpc { get; set; }
    }
}
