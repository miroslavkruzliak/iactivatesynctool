﻿using System;
using System.Collections.Generic;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class AdWordsSitelinkItem
    {
        public long Id { get; set; }
        public int AccountId { get; set; }
        public long GoogleId { get; set; }
        public string Text { get; set; }
        public string Url { get; set; }
        public string Line2 { get; set; }
        public string Line3 { get; set; }
        public int TemplateId { get; set; }
        public DateTime CreatedOn { get; set; }
        public long? DevicePreference { get; set; }
    }
}
