﻿using System;
using System.Collections.Generic;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class AdWordsUnapprovedSitelink
    {
        public long Id { get; set; }
        public long CampaignId { get; set; }
        public long? AdGroupId { get; set; }
        public int AccountId { get; set; }
        public string Text { get; set; }
        public string Url { get; set; }
        public string Line2 { get; set; }
        public string Line3 { get; set; }
        public int TemplateId { get; set; }
        public string Errors { get; set; }
        public DateTime Created { get; set; }
        public bool IsAdWordsError { get; set; }
    }
}
