﻿using System;
using System.Collections.Generic;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class ApiAccount
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public string Path { get; set; }
        public short Api { get; set; }
        public bool Active { get; set; }

        public virtual AdWordsAccount AdWordsAccount { get; set; }
        public virtual BingAccount BingAccount { get; set; }
        public virtual YahooJapanAccount YahooJapanAccount { get; set; }
    }
}
