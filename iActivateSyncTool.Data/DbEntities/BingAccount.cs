﻿using System;
using System.Collections.Generic;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class BingAccount
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string DeveloperToken { get; set; }
        public long CustomerAccountId { get; set; }
        public long CustomerId { get; set; }

        public virtual ApiAccount IdNavigation { get; set; }
    }
}
