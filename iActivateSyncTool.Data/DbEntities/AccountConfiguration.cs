﻿using System;
using System.Collections.Generic;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class AccountConfiguration
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int AccountId { get; set; }
        public string ValueString { get; set; }
        public double? ValueNumber { get; set; }
        public bool Active { get; set; }
    }
}
