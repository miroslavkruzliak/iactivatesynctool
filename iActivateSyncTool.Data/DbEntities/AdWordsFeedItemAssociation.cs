﻿using System;
using System.Collections.Generic;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class AdWordsFeedItemAssociation
    {
        public long Id { get; set; }
        public long FeedItemId { get; set; }
        public int AccountId { get; set; }
        public long AdGroupId { get; set; }
    }
}
