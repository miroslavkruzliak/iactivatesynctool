﻿using System;
using System.Collections.Generic;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class BingKeyword
    {
        public long Id { get; set; }
        public string Text { get; set; }
        public byte MatchingOption { get; set; }
        public string DestinationUrl { get; set; }
        public int? TemplateId { get; set; }
        public int AccountId { get; set; }
        public long CampaignId { get; set; }
        public long AdGroupId { get; set; }
        public long BingId { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsNegative { get; set; }
    }
}
