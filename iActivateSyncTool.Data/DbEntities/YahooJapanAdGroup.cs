﻿using System;
using System.Collections.Generic;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class YahooJapanAdGroup
    {
        public YahooJapanAdGroup()
        {
            YahooJapanAd = new HashSet<YahooJapanAd>();
            YahooJapanKeyword = new HashSet<YahooJapanKeyword>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public long CampaignId { get; set; }
        public int AccountId { get; set; }
        public string AdGroupType { get; set; }
        public long? Cpc { get; set; }
        public int? TemplateId { get; set; }
        public long YahooJapanId { get; set; }
        public DateTime Modified { get; set; }
        public bool IsDisabled { get; set; }
        public bool IsPaused { get; set; }

        public virtual ICollection<YahooJapanAd> YahooJapanAd { get; set; }
        public virtual ICollection<YahooJapanKeyword> YahooJapanKeyword { get; set; }
        public virtual YahooJapanAccount Account { get; set; }
        public virtual YahooJapanCampaign Campaign { get; set; }
    }
}
