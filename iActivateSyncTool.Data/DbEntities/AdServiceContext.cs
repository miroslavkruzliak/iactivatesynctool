﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class AdServiceContext : DbContext
    {
        public AdServiceContext(DbContextOptions options) : base(options)
        {
        }

        public virtual DbSet<AccountConfiguration> AccountConfiguration { get; set; }
        public virtual DbSet<AdWordsAccount> AdWordsAccount { get; set; }
        public virtual DbSet<AdWordsAd> AdWordsAd { get; set; }
        public virtual DbSet<AdWordsAdGroup> AdWordsAdGroup { get; set; }
        public virtual DbSet<AdWordsAdGroupTemplate> AdWordsAdGroupTemplate { get; set; }
        public virtual DbSet<AdWordsAdTemplate> AdWordsAdTemplate { get; set; }
        public virtual DbSet<AdWordsCampaign> AdWordsCampaign { get; set; }
        public virtual DbSet<AdWordsFeedItem> AdWordsFeedItem { get; set; }
        public virtual DbSet<AdWordsFeedItemAssociation> AdWordsFeedItemAssociation { get; set; }
        public virtual DbSet<AdWordsKeyword> AdWordsKeyword { get; set; }
        public virtual DbSet<AdWordsKeywordTemplate> AdWordsKeywordTemplate { get; set; }
        public virtual DbSet<AdWordsSitelink> AdWordsSitelink { get; set; }
        public virtual DbSet<AdWordsSitelinkAssociation> AdWordsSitelinkAssociation { get; set; }
        public virtual DbSet<AdWordsSitelinkItem> AdWordsSitelinkItem { get; set; }
        public virtual DbSet<AdWordsSitelinkTemplate> AdWordsSitelinkTemplate { get; set; }
        public virtual DbSet<AdWordsUnapprovedAd> AdWordsUnapprovedAd { get; set; }
        public virtual DbSet<AdWordsUnapprovedAdGroup> AdWordsUnapprovedAdGroup { get; set; }
        public virtual DbSet<AdWordsUnapprovedKeyword> AdWordsUnapprovedKeyword { get; set; }
        public virtual DbSet<AdWordsUnapprovedSitelink> AdWordsUnapprovedSitelink { get; set; }
        public virtual DbSet<ApiAccount> ApiAccount { get; set; }
        public virtual DbSet<BingAccount> BingAccount { get; set; }
        public virtual DbSet<BingAd> BingAd { get; set; }
        public virtual DbSet<BingAdGroup> BingAdGroup { get; set; }
        public virtual DbSet<BingAdGroupTemplate> BingAdGroupTemplate { get; set; }
        public virtual DbSet<BingAdTemplate> BingAdTemplate { get; set; }
        public virtual DbSet<BingCampaign> BingCampaign { get; set; }
        public virtual DbSet<BingKeyword> BingKeyword { get; set; }
        public virtual DbSet<BingKeywordTemplate> BingKeywordTemplate { get; set; }
        public virtual DbSet<BingUnapprovedAd> BingUnapprovedAd { get; set; }
        public virtual DbSet<BingUnapprovedAdGroup> BingUnapprovedAdGroup { get; set; }
        public virtual DbSet<BingUnapprovedKeyword> BingUnapprovedKeyword { get; set; }
        public virtual DbSet<Tags> Tags { get; set; }
        public virtual DbSet<TaskRuns> TaskRuns { get; set; }
        public virtual DbSet<TaskScheduler> TaskScheduler { get; set; }
        public virtual DbSet<UpdateFeedLog> UpdateFeedLog { get; set; }
        public virtual DbSet<UpdateStatistics> UpdateStatistics { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserPermission> UserPermission { get; set; }
        public virtual DbSet<YahooJapanAccount> YahooJapanAccount { get; set; }
        public virtual DbSet<YahooJapanAd> YahooJapanAd { get; set; }
        public virtual DbSet<YahooJapanAdGroup> YahooJapanAdGroup { get; set; }
        public virtual DbSet<YahooJapanAdGroupTemplate> YahooJapanAdGroupTemplate { get; set; }
        public virtual DbSet<YahooJapanAdTemplate> YahooJapanAdTemplate { get; set; }
        public virtual DbSet<YahooJapanCampaign> YahooJapanCampaign { get; set; }
        public virtual DbSet<YahooJapanKeyword> YahooJapanKeyword { get; set; }
        public virtual DbSet<YahooJapanKeywordTemplate> YahooJapanKeywordTemplate { get; set; }
        public virtual DbSet<YahooJapanUnapprovedAd> YahooJapanUnapprovedAd { get; set; }
        public virtual DbSet<YahooJapanUnapprovedAdGroup> YahooJapanUnapprovedAdGroup { get; set; }
        public virtual DbSet<YahooJapanUnapprovedKeyword> YahooJapanUnapprovedKeyword { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
            //optionsBuilder.UseSqlServer(@"Server=95.138.146.57;Database=AdService;User id=sa;Password=5Ot3GtqfhXhI;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AccountConfiguration>(entity =>
            {
                entity.Property(e => e.Name).IsRequired();

                entity.Property(e => e.ValueString).HasColumnType("varchar(max)");
            });

            modelBuilder.Entity<AdWordsAccount>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AdwordsId).HasColumnType("varchar(max)");

                entity.Property(e => e.AdwordsPass).HasColumnType("varchar(max)");

                entity.Property(e => e.AdwordsToken).HasColumnType("varchar(255)");

                entity.Property(e => e.ClientCustomerId).HasDefaultValueSql("-1");

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.AdWordsAccount)
                    .HasForeignKey<AdWordsAccount>(d => d.Id)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_AdWordsAccount_ApiAccount");
            });

            modelBuilder.Entity<AdWordsAd>(entity =>
            {
                entity.HasIndex(e => new { e.AccountId, e.AdGroupId })
                    .HasName("IX_AdWordsAd_AccountId_AdGroupId");

                entity.HasIndex(e => new { e.AccountId, e.CampaignId })
                    .HasName("IX_AdWordsAd_AccountId_CampaignId");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.DestinationUrl).IsRequired();

                entity.Property(e => e.HeadLine).IsRequired();

                entity.Property(e => e.Line1).IsRequired();

                entity.Property(e => e.Line2).IsRequired();

                entity.Property(e => e.Path1).HasMaxLength(255);

                entity.Property(e => e.Path2).HasMaxLength(255);

                entity.Property(e => e.VisibleUrl).IsRequired();
            });

            modelBuilder.Entity<AdWordsAdGroup>(entity =>
            {
                entity.HasIndex(e => e.AccountId)
                    .HasName("IX_AdWordsAdGroup_AccountId_ON_IsPaused");

                entity.HasIndex(e => new { e.AccountId, e.CampaignId })
                    .HasName("IX_AdWordsAdGroup_AccountId_CampaignId");

                entity.HasIndex(e => new { e.Id, e.AccountId, e.IsDisabled, e.IsPaused })
                    .HasName("IX_AdWordsAdGroup_AccountId_IsDisabled_IsPaused");

                entity.Property(e => e.Modified)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.Name).IsRequired();

                entity.HasOne(d => d.Campaign)
                    .WithMany(p => p.AdWordsAdGroup)
                    .HasForeignKey(d => d.CampaignId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_AdWordsAdGroup_AdWordsCampaign");
            });

            modelBuilder.Entity<AdWordsAdGroupTemplate>(entity =>
            {
                entity.HasIndex(e => e.AccountId)
                    .HasName("IX_AdwordsAdGroupTemplate_AccountId");

                entity.Property(e => e.CampaignName).HasMaxLength(2000);

                entity.Property(e => e.CampaignType).HasMaxLength(255);

                entity.Property(e => e.CreateCondition).HasMaxLength(255);

                entity.Property(e => e.GenerateAdGroupType).HasMaxLength(255);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.RemoveCondition).HasMaxLength(255);

                entity.Property(e => e.Rlsabid)
                    .HasColumnName("RLSABid")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<AdWordsAdTemplate>(entity =>
            {
                entity.HasIndex(e => e.AccountId)
                    .HasName("IX_AdwordsAdTemplate_AccountId");

                entity.Property(e => e.Active).HasDefaultValueSql("1");

                entity.Property(e => e.AdGroupType).HasMaxLength(255);

                entity.Property(e => e.CampaignType).HasMaxLength(255);

                entity.Property(e => e.DestinationUrl)
                    .IsRequired()
                    .HasMaxLength(800);

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.HeadLine)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.IsExclusive).HasDefaultValueSql("0");

                entity.Property(e => e.IsMobilePreferred).HasDefaultValueSql("0");

                entity.Property(e => e.Line1)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Line2)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Path1).HasMaxLength(255);

                entity.Property(e => e.Path2).HasMaxLength(255);

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.VisibleUrl)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<AdWordsCampaign>(entity =>
            {
                entity.HasIndex(e => new { e.AccountId, e.Name })
                    .HasName("IX_AdWordsCampaign_AccountId_Name");

                entity.Property(e => e.CampaignType).HasMaxLength(255);

                entity.Property(e => e.CountryTargeting).HasColumnType("varchar(max)");

                entity.Property(e => e.LanguageTargeting).HasColumnType("varchar(max)");

                entity.Property(e => e.Name).HasMaxLength(255);
            });

            modelBuilder.Entity<AdWordsFeedItem>(entity =>
            {
                entity.HasIndex(e => new { e.AccountId, e.MainCampaignId })
                    .HasName("IX_AdwordsFeedItem_AccountId_MainCampaignId");

                entity.Property(e => e.Values)
                    .IsRequired()
                    .HasColumnType("text");
            });

            modelBuilder.Entity<AdWordsFeedItemAssociation>(entity =>
            {
                entity.HasIndex(e => new { e.AccountId, e.AdGroupId })
                    .HasName("IX_AdwordsFeedItemAssociation_AccountId_AdGroupId");
            });

            modelBuilder.Entity<AdWordsKeyword>(entity =>
            {
                entity.HasIndex(e => new { e.AccountId, e.AdGroupId })
                    .HasName("IX_AdWordsKeywords_AccountId_AdGroupId");

                entity.HasIndex(e => new { e.AccountId, e.CampaignId })
                    .HasName("IX_AdWordsKeyword_AccountId_CampaignId");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsNegative).HasDefaultValueSql("0");

                entity.Property(e => e.MatchingOption).HasDefaultValueSql("0");

                entity.Property(e => e.Text).IsRequired();
            });

            modelBuilder.Entity<AdWordsKeywordTemplate>(entity =>
            {
                entity.HasIndex(e => e.AccountId)
                    .HasName("IX_AdwordsKeywordTemplate_AccountId");

                entity.Property(e => e.Active).HasDefaultValueSql("1");

                entity.Property(e => e.AdGroupName).HasMaxLength(2000);

                entity.Property(e => e.AdGroupType).HasMaxLength(255);

                entity.Property(e => e.CampaignName).HasMaxLength(2000);

                entity.Property(e => e.CampaignType).HasMaxLength(255);

                entity.Property(e => e.Text).HasMaxLength(255);
            });

            modelBuilder.Entity<AdWordsSitelink>(entity =>
            {
                entity.HasIndex(e => new { e.AccountId, e.CampaignId })
                    .HasName("IX_AdWordsSitelink_AccountId_CampaignId");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Text).IsRequired();

                entity.Property(e => e.Url).IsRequired();
            });

            modelBuilder.Entity<AdWordsSitelinkAssociation>(entity =>
            {
                entity.HasIndex(e => new { e.AccountId, e.CampaignId, e.AdGroupId })
                    .HasName("IX_AdWordsSitelinkAssociation_AccountId_CampaignId_AdGroupId");
            });

            modelBuilder.Entity<AdWordsSitelinkItem>(entity =>
            {
                entity.HasIndex(e => e.AccountId)
                    .HasName("IX_AdWordsSitelinkItem_AccountId");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Text).IsRequired();

                entity.Property(e => e.Url).IsRequired();
            });

            modelBuilder.Entity<AdWordsSitelinkTemplate>(entity =>
            {
                entity.HasIndex(e => e.AccountId)
                    .HasName("IX_AdwordsSitelinkTemplate_AccountId");

                entity.Property(e => e.AdGroupName).HasMaxLength(2000);

                entity.Property(e => e.AdGroupType).HasMaxLength(255);

                entity.Property(e => e.CampaignName).HasMaxLength(2000);

                entity.Property(e => e.CampaignType).HasMaxLength(255);

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.Line2).HasMaxLength(255);

                entity.Property(e => e.Line3).HasMaxLength(255);

                entity.Property(e => e.SitelinkType).HasMaxLength(255);

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.Text)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Url).IsRequired();
            });

            modelBuilder.Entity<AdWordsUnapprovedAd>(entity =>
            {
                entity.HasIndex(e => new { e.AccountId, e.IsAdWordsError })
                    .HasName("IX_AdWordsUnapprovedAd_AccountId_IsAdWordsError");

                entity.HasIndex(e => new { e.AccountId, e.CampaignId, e.IsAdWordsError })
                    .HasName("IX_AdWordsUnapprovedAd_AccountId_CampaignId_IsAdWordsError");

                entity.Property(e => e.Created)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.DestinationUrl).IsRequired();

                entity.Property(e => e.Errors).IsRequired();

                entity.Property(e => e.Headline).IsRequired();

                entity.Property(e => e.Line1).IsRequired();

                entity.Property(e => e.Line2).IsRequired();

                entity.Property(e => e.MobileUrl).HasMaxLength(255);

                entity.Property(e => e.Path1).HasMaxLength(255);

                entity.Property(e => e.Path2).HasMaxLength(255);

                entity.Property(e => e.VisibleUrl).IsRequired();
            });

            modelBuilder.Entity<AdWordsUnapprovedAdGroup>(entity =>
            {
                entity.HasIndex(e => new { e.AccountId, e.CampaignId, e.IsAdWordsError })
                    .HasName("IX_AdWordsUnapprovedAdGroup_AccountId_CamapignId_IsAdWordsError");

                entity.Property(e => e.Error).IsRequired();

                entity.Property(e => e.IsAdWordsError).HasDefaultValueSql("0");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<AdWordsUnapprovedKeyword>(entity =>
            {
                entity.HasIndex(e => new { e.AccountId, e.IsAdWordsError })
                    .HasName("IX_AdWordsUnapprovedKeyword_AccountId_IsAdWordsError");

                entity.HasIndex(e => new { e.AccountId, e.CampaignId, e.IsAdWordsError })
                    .HasName("IX_AdWordsUnapprovedKeyword_AccountId_CampaignId_IsAdWordsError");

                entity.Property(e => e.Created)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.Error).IsRequired();

                entity.Property(e => e.IsNegative).HasDefaultValueSql("0");

                entity.Property(e => e.Text).IsRequired();
            });

            modelBuilder.Entity<AdWordsUnapprovedSitelink>(entity =>
            {
                entity.HasIndex(e => new { e.AccountId, e.CampaignId, e.AdGroupId })
                    .HasName("IX_AdWordsUnapprovedSitelink_AccountId_CampaignId_AdGroupId");

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.Property(e => e.Errors).IsRequired();

                entity.Property(e => e.Text).IsRequired();

                entity.Property(e => e.Url).IsRequired();
            });

            modelBuilder.Entity<ApiAccount>(entity =>
            {
                entity.Property(e => e.Password).HasColumnType("varchar(max)");

                entity.Property(e => e.Salt).HasColumnType("varchar(max)");
            });

            modelBuilder.Entity<BingAccount>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Password).IsRequired();

                entity.Property(e => e.Username).IsRequired();

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.BingAccount)
                    .HasForeignKey<BingAccount>(d => d.Id)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_BingAccount_ApiAccount");
            });

            modelBuilder.Entity<BingAd>(entity =>
            {
                entity.HasIndex(e => new { e.AccountId, e.AdGroupId })
                    .HasName("IX_BingAd_AccountId_AdGroupId");

                entity.HasIndex(e => new { e.AccountId, e.CampaignId })
                    .HasName("IX_BingAd_AccountId_CampaignId");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.DestinationUrl).IsRequired();

                entity.Property(e => e.DisplayUrl).IsRequired();

                entity.Property(e => e.Path1).HasMaxLength(255);

                entity.Property(e => e.Path2).HasMaxLength(255);

                entity.Property(e => e.Text).IsRequired();

                entity.Property(e => e.Title).IsRequired();
            });

            modelBuilder.Entity<BingAdGroup>(entity =>
            {
                entity.HasIndex(e => e.AccountId)
                    .HasName("IX_BingAdGroup_AccountId_ON_IsPaused");

                entity.HasIndex(e => new { e.AccountId, e.CampaignId })
                    .HasName("IX_BingAdGroup_AccountId_CampaignId");

                entity.HasIndex(e => new { e.Id, e.AccountId, e.IsDisabled, e.IsPaused })
                    .HasName("IX_BingAdGroup_AccountId_IsDisabled_IsPaused");

                entity.Property(e => e.AdGroupType).HasMaxLength(255);

                entity.Property(e => e.Modified)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.Name).IsRequired();

                entity.HasOne(d => d.Campaign)
                    .WithMany(p => p.BingAdGroup)
                    .HasForeignKey(d => d.CampaignId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_BingAdGroup_BingCampaign");
            });

            modelBuilder.Entity<BingAdGroupTemplate>(entity =>
            {
                entity.HasIndex(e => e.AccountId)
                    .HasName("IX_BingAdGroupTemplate_AccountId");

                entity.Property(e => e.Active).HasDefaultValueSql("1");

                entity.Property(e => e.CampaignName).HasMaxLength(2000);

                entity.Property(e => e.CampaignType).HasMaxLength(255);

                entity.Property(e => e.CreateCondition).HasMaxLength(255);

                entity.Property(e => e.GenerateAdGroupType).HasMaxLength(255);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.RemoveCondition).HasMaxLength(255);
            });

            modelBuilder.Entity<BingAdTemplate>(entity =>
            {
                entity.HasIndex(e => e.AccountId)
                    .HasName("IX_BingAdTemplate_AccountId");

                entity.Property(e => e.Active).HasDefaultValueSql("1");

                entity.Property(e => e.AdGroupName).HasMaxLength(2000);

                entity.Property(e => e.AdGroupType).HasMaxLength(255);

                entity.Property(e => e.CampaignName).HasMaxLength(2000);

                entity.Property(e => e.CampaignType).HasMaxLength(255);

                entity.Property(e => e.DestinationUrl).IsRequired();

                entity.Property(e => e.DisplayUrl).IsRequired();

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.IsExclusive).HasDefaultValueSql("0");

                entity.Property(e => e.IsMobilePreferred).HasDefaultValueSql("0");

                entity.Property(e => e.Path1).HasMaxLength(255);

                entity.Property(e => e.Path2).HasMaxLength(255);

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.Text).IsRequired();

                entity.Property(e => e.Title).IsRequired();
            });

            modelBuilder.Entity<BingCampaign>(entity =>
            {
                entity.HasIndex(e => new { e.AccountId, e.Name })
                    .HasName("IX_BingCampaign_AccountId_Name");

                entity.Property(e => e.CampaignType).HasMaxLength(255);

                entity.Property(e => e.CountryTargeting).HasColumnType("varchar(max)");

                entity.Property(e => e.LanguageTargeting).HasColumnType("varchar(max)");

                entity.Property(e => e.Name).HasMaxLength(255);
            });

            modelBuilder.Entity<BingKeyword>(entity =>
            {
                entity.HasIndex(e => new { e.AccountId, e.AdGroupId })
                    .HasName("IX_BingKeywords_AccountId_AdGroupId");

                entity.HasIndex(e => new { e.AccountId, e.CampaignId })
                    .HasName("IX_BingKeyword_AccountId_CampaignId");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsNegative).HasDefaultValueSql("0");

                entity.Property(e => e.Text).IsRequired();
            });

            modelBuilder.Entity<BingKeywordTemplate>(entity =>
            {
                entity.HasIndex(e => e.AccountId)
                    .HasName("IX_BingKeywordTemplate_AccountId");

                entity.Property(e => e.Active).HasDefaultValueSql("1");

                entity.Property(e => e.AdGroupName).HasMaxLength(2000);

                entity.Property(e => e.AdGroupType).HasMaxLength(255);

                entity.Property(e => e.CampaignName).HasMaxLength(2000);

                entity.Property(e => e.CampaignType).HasMaxLength(255);
            });

            modelBuilder.Entity<BingUnapprovedAd>(entity =>
            {
                entity.HasIndex(e => new { e.AccountId, e.IsBingError })
                    .HasName("IX_BingUnapprovedAd_AccountId_IsBingError");

                entity.HasIndex(e => new { e.AccountId, e.CampaignId, e.IsBingError })
                    .HasName("IX_BingUnapprovedAd_AccountId_CampaignId_IsBingError");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.Errors).IsRequired();

                entity.Property(e => e.Path1).HasMaxLength(255);

                entity.Property(e => e.Path2).HasMaxLength(255);
            });

            modelBuilder.Entity<BingUnapprovedAdGroup>(entity =>
            {
                entity.HasIndex(e => new { e.AccountId, e.CampaignId, e.IsBingError })
                    .HasName("IX_BingUnapprovedAdGroup_AccountId_CamapignId_IsBingError");

                entity.Property(e => e.Error).IsRequired();

                entity.Property(e => e.IsBingError).HasDefaultValueSql("0");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<BingUnapprovedKeyword>(entity =>
            {
                entity.HasIndex(e => new { e.AccountId, e.IsBingError })
                    .HasName("IX_BingUnapprovedKeyword_AccountId_IsBingError");

                entity.HasIndex(e => new { e.AccountId, e.CampaignId, e.IsBingError })
                    .HasName("IX_BingUnapprovedKeyword_AccountId_CampaignId_IsBingError");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Errors).IsRequired();

                entity.Property(e => e.IsNegative).HasDefaultValueSql("0");

                entity.Property(e => e.Text)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Tags>(entity =>
            {
                entity.HasIndex(e => new { e.AccountId, e.CampaignId, e.AdGroupId })
                    .HasName("IX_Tags_AccountId_CampaignId_AdGroupId");

                entity.Property(e => e.TagName)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.TagValue)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<TaskRuns>(entity =>
            {
                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.RunDateTime).HasColumnType("datetime");

                entity.HasOne(d => d.TaskScheduler)
                    .WithMany(p => p.TaskRuns)
                    .HasForeignKey(d => d.TaskSchedulerId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_TaskRuns_TaskScheduler");
            });

            modelBuilder.Entity<TaskScheduler>(entity =>
            {
                entity.Property(e => e.Daily).HasDefaultValueSql("0");

                entity.Property(e => e.DailyAt).HasColumnType("datetime");

                entity.Property(e => e.Enabled).HasDefaultValueSql("1");

                entity.Property(e => e.Immediately).HasDefaultValueSql("NULL");

                entity.Property(e => e.Monthly).HasDefaultValueSql("0");

                entity.Property(e => e.MonthlyAt).HasColumnType("datetime");

                entity.Property(e => e.Name).IsRequired();

                entity.Property(e => e.Timestamp)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.Weekly).HasDefaultValueSql("0");

                entity.Property(e => e.WeeklyAt).HasColumnType("datetime");
            });

            modelBuilder.Entity<UpdateFeedLog>(entity =>
            {
                entity.HasIndex(e => new { e.AccountId, e.Status, e.Date })
                    .HasName("IX_UpdateFeedLog_AccountId_Status_Date");

                entity.Property(e => e.Call).IsRequired();

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.Info).IsRequired();

                entity.Property(e => e.Progress).HasMaxLength(100);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<UpdateStatistics>(entity =>
            {
                entity.HasIndex(e => e.UpdateId)
                    .HasName("IX_UpdateStatistics_UpdateId");

                entity.Property(e => e.Type).IsRequired();

                entity.HasOne(d => d.Update)
                    .WithMany(p => p.UpdateStatistics)
                    .HasForeignKey(d => d.UpdateId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_UpdateStatistics_UpdateFeedLog");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.Active).HasDefaultValueSql("1");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnType("nchar(40)");

                entity.Property(e => e.Salt)
                    .IsRequired()
                    .HasColumnType("nchar(40)");

                entity.Property(e => e.SessionDate).HasColumnType("datetime");

                entity.Property(e => e.SessionId).HasColumnType("nchar(40)");

                entity.Property(e => e.SessionIp)
                    .HasColumnName("SessionIP")
                    .HasMaxLength(15);

                entity.Property(e => e.Username).IsRequired();
            });

            modelBuilder.Entity<UserPermission>(entity =>
            {
                entity.Property(e => e.Active).HasDefaultValueSql("1");

                entity.Property(e => e.Permission).IsRequired();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserPermission)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_UserPermission_User");
            });

            modelBuilder.Entity<YahooJapanAccount>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.ApiAccountId)
                    .IsRequired()
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.ApiAccountPassword).IsRequired();

                entity.Property(e => e.License)
                    .IsRequired()
                    .HasColumnType("varchar(200)");

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.YahooJapanAccount)
                    .HasForeignKey<YahooJapanAccount>(d => d.Id)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_YahooJapanAccount_ApiAccount");
            });

            modelBuilder.Entity<YahooJapanAd>(entity =>
            {
                entity.HasIndex(e => new { e.AccountId, e.AdGroupId })
                    .HasName("IX_YahooJapanAd_AccountId_AdGroupId");

                entity.HasIndex(e => new { e.AccountId, e.YahooJapanId })
                    .HasName("IX_YahooJapanAd_AccountId_YahooJapanId");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.Description1).IsRequired();

                entity.Property(e => e.Description2).IsRequired();

                entity.Property(e => e.DestinationUrl).IsRequired();

                entity.Property(e => e.DisplayUrl).IsRequired();

                entity.Property(e => e.HeadLine).IsRequired();

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.YahooJapanAd)
                    .HasForeignKey(d => d.AccountId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_YahooJapanAd_YahooJapanAccount");

                entity.HasOne(d => d.AdGroup)
                    .WithMany(p => p.YahooJapanAd)
                    .HasForeignKey(d => d.AdGroupId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_YahooJapanAd_YahooJapanAdGroup");

                entity.HasOne(d => d.Campaign)
                    .WithMany(p => p.YahooJapanAd)
                    .HasForeignKey(d => d.CampaignId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_YahooJapanAd_YahooJapanCampaign");
            });

            modelBuilder.Entity<YahooJapanAdGroup>(entity =>
            {
                entity.HasIndex(e => e.AccountId)
                    .HasName("IX_YahooJapanAdGroup_AccountId_ON_IsPaused");

                entity.HasIndex(e => new { e.AccountId, e.CampaignId })
                    .HasName("IX_YahooJapanAdGroup_AccountId_CampaignId");

                entity.HasIndex(e => new { e.AccountId, e.YahooJapanId })
                    .HasName("IX_YahooJapanAdGroup_AccountId_YahooJapanId");

                entity.Property(e => e.Modified)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.Name).IsRequired();

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.YahooJapanAdGroup)
                    .HasForeignKey(d => d.AccountId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_YahooJapanAdGroup_YahooJapanAccount");

                entity.HasOne(d => d.Campaign)
                    .WithMany(p => p.YahooJapanAdGroup)
                    .HasForeignKey(d => d.CampaignId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_YahooJapanAdGroup_YahooJapanCampaign");
            });

            modelBuilder.Entity<YahooJapanAdGroupTemplate>(entity =>
            {
                entity.HasIndex(e => e.AccountId)
                    .HasName("IX_YahooJapanAdGroupTemplate_AccountId");

                entity.Property(e => e.Active).HasDefaultValueSql("1");

                entity.Property(e => e.CampaignName).HasMaxLength(2000);

                entity.Property(e => e.CampaignType).HasMaxLength(255);

                entity.Property(e => e.CreateCondition).HasMaxLength(255);

                entity.Property(e => e.GenerateAdGroupType).HasMaxLength(255);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.RemoveCondition).HasMaxLength(255);

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.YahooJapanAdGroupTemplate)
                    .HasForeignKey(d => d.AccountId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_YahooJapanAdGroupTemplate_YahooJapanAccount");

                entity.HasOne(d => d.Campaign)
                    .WithMany(p => p.YahooJapanAdGroupTemplate)
                    .HasForeignKey(d => d.CampaignId)
                    .HasConstraintName("FK_YahooJapanAdGroupTemplate_YahooJapanCampaign");
            });

            modelBuilder.Entity<YahooJapanAdTemplate>(entity =>
            {
                entity.HasIndex(e => e.AccountId)
                    .HasName("IX_YahooJapanAdTemplate_AccountId");

                entity.Property(e => e.Active).HasDefaultValueSql("1");

                entity.Property(e => e.AdGroupName).HasMaxLength(2000);

                entity.Property(e => e.AdGroupType).HasMaxLength(255);

                entity.Property(e => e.CampaignName).HasMaxLength(2000);

                entity.Property(e => e.CampaignType).HasMaxLength(255);

                entity.Property(e => e.Description1)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Description2)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.DestinationUrl)
                    .IsRequired()
                    .HasMaxLength(800);

                entity.Property(e => e.DisplayUrl)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.HeadLine)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.IsExclusive).HasDefaultValueSql("0");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.YahooJapanAdTemplate)
                    .HasForeignKey(d => d.AccountId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_YahooJapanAdTemplate_YahooJapanAccount");

                entity.HasOne(d => d.AlternativeToNavigation)
                    .WithMany(p => p.InverseAlternativeToNavigation)
                    .HasForeignKey(d => d.AlternativeTo)
                    .HasConstraintName("FK_YahooJapanAdTemplate_YahooJapanAdTemplate");
            });

            modelBuilder.Entity<YahooJapanCampaign>(entity =>
            {
                entity.HasIndex(e => new { e.AccountId, e.Name })
                    .HasName("IX_YahooJapanCampaign_AccountId_Name");

                entity.Property(e => e.CampaignType).HasMaxLength(255);

                entity.Property(e => e.Name).HasMaxLength(255);

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.YahooJapanCampaign)
                    .HasForeignKey(d => d.AccountId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_YahooJapanCampaign_YahooJapanAccount");
            });

            modelBuilder.Entity<YahooJapanKeyword>(entity =>
            {
                entity.HasIndex(e => e.AdGroupId)
                    .HasName("IX_YahooJapanKeyword_AdGroupId");

                entity.HasIndex(e => new { e.AccountId, e.AdGroupId })
                    .HasName("IX_YahooJapanKeyword_AccountId_AdGroupId");

                entity.HasIndex(e => new { e.AccountId, e.CampaignId })
                    .HasName("IX_YahooJapanKeyword_AccountId_CampaignId");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.MatchingOption).HasDefaultValueSql("0");

                entity.Property(e => e.Text).IsRequired();

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.YahooJapanKeyword)
                    .HasForeignKey(d => d.AccountId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_YahooJapanKeyword_YahooJapanAccount");

                entity.HasOne(d => d.AdGroup)
                    .WithMany(p => p.YahooJapanKeyword)
                    .HasForeignKey(d => d.AdGroupId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_YahooJapanKeyword_YahooJapanAdGroup");

                entity.HasOne(d => d.Campaign)
                    .WithMany(p => p.YahooJapanKeyword)
                    .HasForeignKey(d => d.CampaignId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_YahooJapanKeyword_YahooJapanCampaign");
            });

            modelBuilder.Entity<YahooJapanKeywordTemplate>(entity =>
            {
                entity.HasIndex(e => e.AccountId)
                    .HasName("IX_YahooJapanKeywordTemplate_AccountId");

                entity.Property(e => e.Active).HasDefaultValueSql("1");

                entity.Property(e => e.AdGroupName).HasMaxLength(2000);

                entity.Property(e => e.AdGroupType).HasMaxLength(255);

                entity.Property(e => e.CampaignName).HasMaxLength(2000);

                entity.Property(e => e.CampaignType).HasMaxLength(255);

                entity.Property(e => e.Text).HasMaxLength(255);

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.YahooJapanKeywordTemplate)
                    .HasForeignKey(d => d.AccountId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_YahooJapanKeywordTemplate_YahooJapanAccount");
            });

            modelBuilder.Entity<YahooJapanUnapprovedAd>(entity =>
            {
                entity.HasIndex(e => new { e.AccountId, e.IsYahooJapanError })
                    .HasName("IX_YahooJapanUnapprovedAd_AccountId_IsYahooJapanError");

                entity.HasIndex(e => new { e.AccountId, e.CampaignId, e.IsYahooJapanError })
                    .HasName("IX_YahooJapanUnapprovedAd_AccountId_CampaignId_IsYahooJapanError");

                entity.Property(e => e.Created)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.Description1).IsRequired();

                entity.Property(e => e.Description2).IsRequired();

                entity.Property(e => e.DestinationUrl).IsRequired();

                entity.Property(e => e.DisplayUrl).IsRequired();

                entity.Property(e => e.Errors).IsRequired();

                entity.Property(e => e.Headline).IsRequired();

                entity.Property(e => e.IsYahooJapanError).HasDefaultValueSql("0");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.YahooJapanUnapprovedAd)
                    .HasForeignKey(d => d.AccountId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_YahooJapanUnapprovedAd_YahooJapanAccount");
            });

            modelBuilder.Entity<YahooJapanUnapprovedAdGroup>(entity =>
            {
                entity.HasIndex(e => new { e.AccountId, e.CampaignId })
                    .HasName("IX_YahooJapanUnapprovedAdGroup_AccountId_CampaignId");

                entity.Property(e => e.Error).IsRequired();

                entity.Property(e => e.IsYahooJapanError).HasDefaultValueSql("0");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.YahooJapanUnapprovedAdGroup)
                    .HasForeignKey(d => d.AccountId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_YahooJapanUnapprovedAdGroup_YahooJapanAccount");

                entity.HasOne(d => d.Campaign)
                    .WithMany(p => p.YahooJapanUnapprovedAdGroup)
                    .HasForeignKey(d => d.CampaignId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_YahooJapanUnapprovedAdGroup_YahooJapanCampaign");
            });

            modelBuilder.Entity<YahooJapanUnapprovedKeyword>(entity =>
            {
                entity.HasIndex(e => new { e.AccountId, e.IsYahooJapanError })
                    .HasName("IX_YahooJapanUnapprovedKeyword_AccountId_IsYahooJapanError");

                entity.HasIndex(e => new { e.AccountId, e.CampaignId, e.IsYahooJapanError })
                    .HasName("IX_YahooJapanUnapprovedKeyword_AccountId_CampaignId_IsYahooJapanError");

                entity.Property(e => e.Created)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.Error).IsRequired();

                entity.Property(e => e.IsYahooJapanError).HasDefaultValueSql("0");

                entity.Property(e => e.Text).IsRequired();

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.YahooJapanUnapprovedKeyword)
                    .HasForeignKey(d => d.AccountId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_YYahooJapanUnapprovedKeyword_YahooJapanAccount");
            });
        }
    }
}