﻿using System;
using System.Collections.Generic;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class YahooJapanKeyword
    {
        public long Id { get; set; }
        public string Text { get; set; }
        public byte MatchingOption { get; set; }
        public int? TemplateId { get; set; }
        public int AccountId { get; set; }
        public long AdGroupId { get; set; }
        public long YahooJapanId { get; set; }
        public DateTime CreatedOn { get; set; }
        public string DestinationUrl { get; set; }
        public long CampaignId { get; set; }

        public virtual YahooJapanAccount Account { get; set; }
        public virtual YahooJapanAdGroup AdGroup { get; set; }
        public virtual YahooJapanCampaign Campaign { get; set; }
    }
}
