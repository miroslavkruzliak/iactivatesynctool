﻿using System;
using System.Collections.Generic;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class User
    {
        public User()
        {
            UserPermission = new HashSet<UserPermission>();
        }

        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public string Email { get; set; }
        public bool Active { get; set; }
        public string SessionId { get; set; }
        public DateTime? SessionDate { get; set; }
        public string SessionIp { get; set; }

        public virtual ICollection<UserPermission> UserPermission { get; set; }
    }
}
