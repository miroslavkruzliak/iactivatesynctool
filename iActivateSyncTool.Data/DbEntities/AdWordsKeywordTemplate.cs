﻿using System;
using System.Collections.Generic;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class AdWordsKeywordTemplate
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public int AccountId { get; set; }
        public long? CampaignId { get; set; }
        public long? AdGroupId { get; set; }
        public string CampaignType { get; set; }
        public string AdGroupType { get; set; }
        public bool Active { get; set; }
        public string DestinationUrl { get; set; }
        public string Rule { get; set; }
        public string CampaignName { get; set; }
        public string AdGroupName { get; set; }
    }
}
