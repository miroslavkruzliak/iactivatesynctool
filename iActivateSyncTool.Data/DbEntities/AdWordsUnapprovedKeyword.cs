﻿using System;
using System.Collections.Generic;

namespace iActivateSyncTool.Data.DbEntities
{
    public partial class AdWordsUnapprovedKeyword
    {
        public long Id { get; set; }
        public long AdGroupId { get; set; }
        public long CampaignId { get; set; }
        public int AccountId { get; set; }
        public string Text { get; set; }
        public int? TemplateId { get; set; }
        public string Error { get; set; }
        public DateTime Created { get; set; }
        public bool IsAdWordsError { get; set; }
        public string DestinationUrl { get; set; }
        public byte MatchingOption { get; set; }
        public bool IsNegative { get; set; }
    }
}
