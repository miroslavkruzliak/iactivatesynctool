﻿using iActivateSyncTool.Data.ApiEntities;
using System;
using System.Collections.Generic;
using System.Text;
using iActivateSyncTool.Logic.Helpers;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.IO;
using System.Linq;

namespace iActivateSyncTool.Logic.Parsers
{
    public class AdWordsCampaignReportParser : IReportParser<ApiCampaign>
    {
        public async Task<ApiCampaign[]> ParseAsync(string reportPath)
        {
            using (var stream = File.OpenRead(reportPath))
            {
                XDocument doc = await XDocument.LoadAsync(stream, LoadOptions.PreserveWhitespace, System.Threading.CancellationToken.None);
                return doc.Element("report").Element("table").Elements("row").Select(e => new ApiCampaign
                {
                    ApiId = long.Parse(e.Attribute("campaignID").Value),
                    Name = e.Attribute("campaign").Value,
                    Status = e.Attribute("campaignState").Value
                }).ToArray();
            }
        }
    }
}
