﻿using iActivateSyncTool.Logic.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace iActivateSyncTool.Logic.Parsers
{
    interface IReportParser<T>
    {
        Task<T[]> ParseAsync(string reportPath);
    }
}
