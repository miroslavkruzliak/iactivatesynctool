﻿using iActivateSyncTool.Data.ApiEntities;
using iActivateSyncTool.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iActivateSyncTool.Logic.Comparers
{
    public abstract class BaseComparer<D, Db, Api>
                        where Db : IDbEntity
                        where Api : IApiEntity
                        where D : struct
    {
        
        public virtual IDbEntity[] GetApiMissing(Db[] dbEntities, Api[] apiEntities)
        {
            var apiIds = apiEntities.Select(a => a.ApiId).ToArray();
            return dbEntities.Where(d => !apiIds.Contains(d.ApiId)).Select(d => (IDbEntity)d).ToArray();
        }

        public virtual IApiEntity[] GetDBMissing(Db[] dbEntities, Api[] apiEntities)
        {
            var apiIds = dbEntities.Select(a => a.ApiId).ToArray();
            return apiEntities.Where(d => !apiIds.Contains(d.ApiId)).Select(a => (IApiEntity)a).ToArray();
        }

        public virtual Result<D>[] GetDifferences(Db[] dbEntities, Api[] apiEntities)
        {
            // items which are present in DB and also in API
            return dbEntities.Join(apiEntities, d => d.ApiId, a => a.ApiId, (d, a) => Compare(d, a)).ToArray();
        }

        protected abstract Result<D> Compare(Db dbEntity, Api apiEntity);
    }
}
