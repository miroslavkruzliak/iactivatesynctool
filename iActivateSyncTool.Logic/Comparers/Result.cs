﻿using iActivateSyncTool.Data.ApiEntities;
using iActivateSyncTool.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace iActivateSyncTool.Logic.Comparers
{
    public sealed class Result<D>
    {
        public IApiEntity ApiEntity { get; set; }
        public IDbEntity DbEntity { get; set; }

        public D[] Differences { get; set; }
        
    }
}
