﻿using iActivateSyncTool.Data.ApiEntities;
using iActivateSyncTool.Data.DbEntities;
using iActivateSyncTool.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iActivateSyncTool.Logic.Comparers
{

    public class CampaignComparer : BaseComparer<CampaignDifference, IDbCampaign, ApiCampaign>
    {
        public CampaignComparer()
        {
        }

        protected override Result<CampaignDifference> Compare(IDbCampaign dbEntity, ApiCampaign apiEntity)
        {
            var result = new Result<CampaignDifference> { ApiEntity = apiEntity, DbEntity = dbEntity };
            result.Differences = (apiEntity.Name != dbEntity.Name)?  new CampaignDifference[] { CampaignDifference.Name } : new CampaignDifference[0];
            return result;
        }
    }
}
