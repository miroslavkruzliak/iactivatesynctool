﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iActivateSyncTool.Logic.Helpers
{
    public enum SortDirection
    {
        ASC,
        DESC
    }
    public class PagingOptions
    {
        public string Sort { get; set; }
        public SortDirection SortDirection { get; set; }
        public int Page { get; set; }
        public int ItemsPerPage { get; set; }
        public int TotalItems { get; set; }
    }
}
