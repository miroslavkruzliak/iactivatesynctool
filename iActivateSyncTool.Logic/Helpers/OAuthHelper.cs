﻿using iActivateSyncTool.Data;
using iProspect.OAuth;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace iActivateSyncTool.Logic.Helpers
{
    public static class OAuthHelper
    {

        private const string OAUTH_USER = "iprospect";
        private const string OAUTH_PASSWD = "FWhD50avChcI";

        public static Task<TokensInfo> GetTokensAsync(string user, ApiType apiType)
        {
            OAuthProxy oauthProxy = new OAuthProxy(OAUTH_USER, OAUTH_PASSWD);
            switch (apiType)
            {
                case ApiType.AdWords:
                    return oauthProxy.GetGoogleTokens(user);
                case ApiType.BingAds:
                    return oauthProxy.GetMicrosoftTokens(user);
            }

            throw new ApplicationException("Not supported API type!");
        }
        
    }
}
