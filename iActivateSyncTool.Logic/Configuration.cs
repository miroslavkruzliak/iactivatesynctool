﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace iActivateSyncTool.Logic
{
    public class Configuration
    {
        private static Configuration instance = null;
        private static IConfigurationBuilder builder = null;
        static Configuration()
        {
            builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
        }

        public static Configuration Instance
        {
            get
            {
                if (instance == null)
                    Initialize(builder);

                return instance;
            }
        }

        private static void Initialize(IConfigurationBuilder builder)
        {
            var confRoot = builder.Build();
            instance = new Configuration
            {
                ReportsDirectory = confRoot["AppSettings:ReportsDirectory"]
            };
        }

        public string ReportsDirectory { get; private set; }
    }
}
