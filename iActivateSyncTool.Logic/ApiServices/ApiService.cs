﻿using iActivateSyncTool.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace iActivateSyncTool.Logic.ApiServices
{
    public class ApiService
    {
        private ApiType apiType;
        public ApiService(ApiType apiType)
        {
            this.apiType = apiType;
        }

        protected string CampaignReportDirectoryPath { get => Path.Combine(Configuration.Instance.ReportsDirectory, this.apiType.ToString(), "Campaigns"); }
        protected string AdGroupReportDirectoryPath { get => Path.Combine(Configuration.Instance.ReportsDirectory, this.apiType.ToString(), "AdGroups"); }
        protected string AdReportDirectoryPath { get => Path.Combine(Configuration.Instance.ReportsDirectory, this.apiType.ToString(), "Ads"); }
    }
}
