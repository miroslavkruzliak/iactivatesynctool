﻿using iProspect.OAuth;
using System;
using System.Threading.Tasks;
using System.Xml.Linq;
using iActivateSyncTool.Data.ApiEntities;
using System.Net.Http;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Collections.Generic;
using iActivateSyncTool.Logic.Parsers;
using iActivateSyncTool.Logic.Helpers;
using iActivateSyncTool.Data;

namespace iActivateSyncTool.Logic.ApiServices
{
    public class AdWordsService : ApiService, IApiService
    {
        private const string DEVELOPER_TOKEN = "8MQ2891hEAbOZ_Szp5x0fw";
        private const string ADWORDS_USER = "apinetsociety2@gmail.com";
        private const string REPORT_VERSION = "v201708";

        public AdWordsService() : base(ApiType.AdWords)
        {
        }

        public Task<ApiCampaign[]> GetApiAdGroupsAsync()
        {
            throw new NotImplementedException();
        }

        public Task<ApiCampaign[]> GetApiAdsAsync()
        {
            throw new NotImplementedException();
        }

        public async Task<ApiCampaign[]> GetApiCampaignsAsync(long ccid)
        {
            Selector sel = new Selector
            {
                predicates = new Predicate[] {
                    new Predicate { field="CampaignName", @operator=PredicateOperator.CONTAINS_IGNORE_CASE, values = new string[] { "API" } },
                    new Predicate { field="CampaignStatus", @operator=PredicateOperator.NOT_IN, values = new string[] { "REMOVED" } }
                },
                fields = new string[] { "CampaignId",
                                        "CampaignName",
                                        "CampaignStatus" }
            };

            var def = new reportDefinition
            {
                dateRangeType = ReportDefinitionDateRangeType.ALL_TIME,
                reportType = ReportDefinitionReportType.CAMPAIGN_PERFORMANCE_REPORT,
                downloadFormat = DownloadFormat.XML,
                selector = sel,
                reportName = "Sync tool report " + DateTime.Now
            };

            var reportPath = Path.Combine(this.CampaignReportDirectoryPath, $"{ccid}.xml");

            await this.GetReport(def, reportPath, ccid);

            return await new AdWordsCampaignReportParser().ParseAsync(reportPath);
        }


        private async Task GetReport(reportDefinition reportDefinition, string reportPath, long ccid)
        {
            FileInfo report = new FileInfo(reportPath);
            if (report.Exists && ((DateTime.UtcNow - report.LastWriteTimeUtc).TotalHours < 1))
                return;

            var tokens = await OAuthHelper.GetTokensAsync(ADWORDS_USER, Data.ApiType.AdWords);
            HttpClient client = new HttpClient();

            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri($"https://adwords.google.com/api/adwords/reportdownload/{REPORT_VERSION}"),
                Method = HttpMethod.Post
            };
            request.Headers.Add("Authorization", $"Bearer {tokens.AccessToken}");
            request.Headers.Add("developerToken", DEVELOPER_TOKEN);
            request.Headers.Add("clientCustomerId", ccid.ToString());

            string content = null;
            XmlSerializer serializer = new XmlSerializer(typeof(reportDefinition));
            using (var sww = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(sww, new XmlWriterSettings { OmitXmlDeclaration = true }))
                {
                    serializer.Serialize(writer, reportDefinition);
                    content = sww.ToString();
                }
            }

            var dict = new Dictionary<string, string> { ["__rdxml"] = content };

            request.Content = new FormUrlEncodedContent(dict);
            request.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/x-www-form-urlencoded");

            Directory.CreateDirectory(report.Directory.FullName);

            using (var response = await client.SendAsync(request))
            using (var fileStream = File.OpenWrite(reportPath))
            {
                await response.Content.CopyToAsync(fileStream);
            }
        }
    }
}
