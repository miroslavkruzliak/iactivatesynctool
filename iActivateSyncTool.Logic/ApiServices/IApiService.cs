﻿using iActivateSyncTool.Data.ApiEntities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace iActivateSyncTool.Logic.ApiServices
{
    public interface IApiService
    {
        Task<ApiCampaign[]> GetApiCampaignsAsync(long accountId);
        Task<ApiCampaign[]> GetApiAdGroupsAsync();
        Task<ApiCampaign[]> GetApiAdsAsync();
    }
}
