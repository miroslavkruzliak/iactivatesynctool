﻿using iActivateSyncTool.Data.ApiEntities;
using iActivateSyncTool.Data.Interfaces;
using iActivateSyncTool.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iActivateSyncTool.ViewModels
{
    public class CampaignViewModel
    {
        public IDbCampaign DbCampaign { get; set; }
        public ApiCampaign ApiCampaign { get; set; }
        public CampaignDifference[] Differences { get; set; }
    }
}
