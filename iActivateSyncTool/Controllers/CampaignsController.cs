﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using iActivateSyncTool.Data;
using iActivateSyncTool.Services;

namespace iActivateSyncTool.Controllers
{
    public class CampaignsController : Controller
    {
        private ICampaignsService campaignsService;
        public CampaignsController(ICampaignsService campaignsService)
        {
            this.campaignsService = campaignsService;
        }
        public async Task<IActionResult> Differences(int accountId, ApiType apiType)
        {
            return new JsonResult(await campaignsService.GetCampaignDifferencesAsync(accountId, apiType));
        }

        public async Task<IActionResult> DbMissing(int accountId, ApiType apiType)
        {
            return new JsonResult(await campaignsService.GetMissingDbCampaignsAsync(accountId, apiType));
        }

        public async Task<IActionResult> ApiMissing(int accountId, ApiType apiType)
        {
            return new JsonResult(await campaignsService.GetMissingApiCampaignsAsync(accountId, apiType));
        }


    }
}