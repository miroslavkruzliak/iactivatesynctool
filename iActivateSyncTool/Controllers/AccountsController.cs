using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using iActivateSyncTool.Services;
using iActivateSyncTool.ViewModels;

namespace iActivateSyncTool.Controllers
{
    public class AccountsController : Controller
    {
        private IAccountsService accountService;
        public AccountsController(IAccountsService accountService)
        {
            this.accountService = accountService;
        }

        public async Task<IActionResult> List()
        {
            return new JsonResult(await accountService.GetAccountsAsync());
        }
    }
}