﻿import { Injectable, Pipe, PipeTransform } from "@angular/core"

@Pipe({
    name: 'search'
})

@Injectable()
export class SearchPipe implements PipeTransform {
    transform(items: any[], value: string): any[] {
        if (!items) return [];
        if (!value) return items;

        let regex = new RegExp(value, "gi");

        return items.filter(item => {
            for (let key in item)
                if (item[key])
                    if (regex.test(item[key].toString()))
                        return true;

            return false;
        });
    }
}