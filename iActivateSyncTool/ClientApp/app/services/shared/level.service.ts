﻿import { Injectable } from '@angular/core';
import { Level } from '../../models/Level'
import { Subject } from 'rxjs/Subject';

@Injectable()
export class LevelService {
    // Observable sources
    private levelSource = new Subject<Level>();

    // Observable streams
    levelChanged$ = this.levelSource.asObservable();

    onLevelChange(level: Level): void {
        this.levelSource.next(level);
    }

}