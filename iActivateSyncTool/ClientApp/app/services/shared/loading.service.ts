﻿import { Injectable } from '@angular/core'
import { Subscription } from 'rxjs/Subscription'
import { Subject } from 'rxjs/Subject'

@Injectable()
export class LoadingService {
    // list of subscriptions, that caused loading
    private subscriptions: Subscription[] = [];

    // resource of booleans indicating if the loading view should appear
    private loadingResoourse: Subject<boolean> = new Subject<boolean>();

    // exposing property for Observers to get the stream
    $loading = this.loadingResoourse.asObservable();

    public load(subscription: Subscription): void {
        
        this.loadingResoourse.next(true);
        subscription.add(() => {
            let index = this.subscriptions.indexOf(subscription);
            if (index < 0)
                return;
            this.subscriptions.splice(index, 1);
            if (this.subscriptions.length == 0)
                this.loadingResoourse.next(false);
        });
        this.subscriptions.push(subscription);
    }
}