﻿import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Alert } from '../../models/Alert';

@Injectable()
export class AlertService {
    // Observable sources
    private alertSource = new Subject<Alert>();

    // Observable streams
    alertChanged$ = this.alertSource.asObservable();

    public alert(alert: Alert) {
        this.alertSource.next(alert);
    }
}