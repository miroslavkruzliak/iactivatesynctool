﻿import { Injectable } from "@angular/core"
import { Observable } from "rxjs/Observable";
import { Http } from '@angular/http';

import { ApiType } from '../models/Account';
import { Campaign } from '../models/Campaign';
import { DifferenceType } from '../models/DifferenceType';

@Injectable()
export class CampaignsService {

    constructor(private http: Http) {
    }

    getCampaignsAsync(accountId: number, apiType: ApiType, difference: DifferenceType): Observable<Campaign[]> {
        return this.http.get(`/${accountId}/${ApiType[apiType]}/campaigns/${DifferenceType[difference]}`)
            .map(res => res.json() as Campaign[])

    }
}