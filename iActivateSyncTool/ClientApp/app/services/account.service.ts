﻿import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Account, ApiType } from '../models/Account';
import { Observable } from "rxjs/Observable";
import { Subject } from 'rxjs/Subject';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class AccountService {

    // Observable sources
    private accountSource = new Subject<Account>();

    // Observable streams
    accountChanged$ = this.accountSource.asObservable();

    constructor(private http: Http) {
    }

    getAccountsAsync(type?: ApiType): Observable<Account[]> {
        return this.http.get("/accounts")
            .map(res => res.json() as Account[])
           
    }

    // Service message commands
    selectAccount(account: Account) {
        this.accountSource.next(account);
    }
}
