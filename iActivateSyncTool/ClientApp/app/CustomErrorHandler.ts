﻿import { ErrorHandler, Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { AlertService } from './services/shared/alert.service';
import { Alert, AlertType } from './models/Alert';

@Injectable()
export class CustomErrorHandler extends ErrorHandler {

    constructor(private alertService: AlertService) {
        super();
    }

    handleError(error: any) {
        if (error instanceof Response) {
            let serverError = (error as Response).json();
            this.alertService.alert(new Alert(serverError.Error, AlertType.Error));
        }
    }
}