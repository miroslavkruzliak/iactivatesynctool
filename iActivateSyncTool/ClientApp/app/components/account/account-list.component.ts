﻿import { Component, OnInit, Input } from '@angular/core';
import { NgIf } from '@angular/common';
import { Account } from '../../models/Account';
import { AccountService } from '../../services/account.service';
import { LoadingService } from '../../services/shared/loading.service';
import { BaseListComponent } from '../base-list.component';

@Component({
    templateUrl: "./account-list.component.html",
    selector: "account-list",
})
export class AccountListComponent extends BaseListComponent<Account> implements OnInit {

    @Input()
    searchValue: string;

    constructor(private accountService: AccountService,
        loadingService: LoadingService) {
        super(loadingService);
    }

    ngOnInit() {
        this.isLoading = this.accountService.getAccountsAsync().subscribe(accounts => this.list = accounts);
    }


    public selectAccount(account: Account): void {
        // let know other components that the account was set
        this.accountService.selectAccount(account);
    }

    
}