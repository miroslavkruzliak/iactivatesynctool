﻿import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router'
import { NgIf } from '@angular/common';

import { Campaign } from '../../models/Campaign';
import { LoadingService } from '../../services/shared/loading.service';
import { BaseListComponent } from '../base-list.component';

import 'rxjs/add/operator/switchMap';
import { Observable } from 'rxjs/Observable';

@Component({
    templateUrl: "./campaign-list.component.html",
    selector: "campaign-list",
})

export class CampaignListComponent extends BaseListComponent<Campaign> {

    constructor(loadingService: LoadingService, private route: ActivatedRoute) {
        super(loadingService);

        console.log(route);
    }
}