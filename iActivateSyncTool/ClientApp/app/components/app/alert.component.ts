﻿import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { AlertService } from '../../services/shared/alert.service';
import { Alert, AlertType } from '../../models/Alert';

@Component({
    selector: 'alerts',
    templateUrl: './alert.component.html'
})
export class AlertComponent implements OnInit {

    public alerts: Array<Alert> = new Array<Alert>();

    constructor(private alertService: AlertService, private ref: ChangeDetectorRef) {
        
    }

    ngOnInit() {
        this.alertService.alertChanged$.subscribe(alert => {
            this.alerts.push(alert);
            this.ref.detectChanges();
        });
    }


    removeAlert(alert: Alert) {
        this.alerts = this.alerts.filter(x => x !== alert);
    }

    cssClass(alert: Alert) {
        if (!alert) {
            return;
        }

        // return css class based on alert type
        switch (alert.type) {
            case AlertType.Success:
                return 'alert alert-success';
            case AlertType.Error:
                return 'alert alert-danger';
            case AlertType.Info:
                return 'alert alert-info';
            case AlertType.Warning:
                return 'alert alert-warning';
        }
    }

}