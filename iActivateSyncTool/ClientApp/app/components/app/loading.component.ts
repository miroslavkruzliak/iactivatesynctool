﻿import { Component, OnInit } from '@angular/core';
import { LoadingService } from '../../services/shared/loading.service';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
 
@Component({
    template: "<div *ngIf='show' class='loading'><mat-spinner color='accent' class='loader'></mat-spinner></div>",
    selector: "loading",
    styleUrls: ["./loading.component.css"]
})
export class LoadingComponent implements OnInit {
    show: boolean = false;

    constructor(private loadingService: LoadingService) {
    }

    ngOnInit() {
        this.loadingService.$loading.subscribe(value => this.show = value);
    }
}