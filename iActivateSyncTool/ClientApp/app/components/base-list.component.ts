﻿import { Input } from "@angular/core"
import { BaseComponent } from "./base.component"
import { LoadingService } from "../services/shared/loading.service"

export class BaseListComponent<T> extends BaseComponent {

    constructor(loadingService: LoadingService) {
        super(loadingService);
    }

    @Input()
    public searchValue: string;
    public list: T[];
}