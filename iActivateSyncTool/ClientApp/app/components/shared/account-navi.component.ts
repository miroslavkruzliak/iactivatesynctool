﻿import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgIf, NgForOf } from '@angular/common';
import { DifferenceType } from '../../models/DifferenceType';
import { Level } from '../../models/Level';
import { Account, ApiType } from '../../models/Account';

@Component({
    templateUrl: "./account-navi.component.html",
    selector: "account-navi",
    styleUrls: ["./account-navi.component.css"]
})
export class AccountNaviComponent implements OnInit {

    private _account: Account;
    // defining input account for the component neccessary for navigation
    @Input()
    set account(account: Account) {
        this._account = account;
        this.activeDifference = DifferenceType.Difference;
        this.activeLevel = undefined;
    }

    get account(): Account {
        return this._account;
    };

    // types needed in template
    differenceTypes = DifferenceType;
    levelTypes = Level;

    differenceTypesValues: string[];
    levelTypesValues: string[];

    // active properties
    activeLevel?: Level;
    activeDifference: DifferenceType = DifferenceType.Difference;

    constructor(private router: Router) {
    }

    ngOnInit() {
        let types = Object.keys(DifferenceType);
        this.differenceTypesValues = types.slice(types.length / 2);
        types = Object.keys(Level);
        this.levelTypesValues = types.slice(types.length / 2);
        this.activeLevel
    }


    linkClick(level: Level, difference: DifferenceType = DifferenceType.Difference): void {
        if (!this.account)
            return;
        this.activeLevel = level;
        this.activeDifference = difference;
        this.router.navigate([this.account.id, ApiType[this.account.apiType], Level[level], DifferenceType[difference]]);
    }
}