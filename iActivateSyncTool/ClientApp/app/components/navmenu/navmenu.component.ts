import { Component } from '@angular/core';
import { NgIf } from '@angular/common';
import { Account, ApiType } from '../../models/Account';
import { Level } from '../../models/Level';
import { AccountListComponent } from '../../components/account/account-list.component'; 
import { AccountService } from '../../services/account.service';
import { LevelService } from '../../services/shared/level.service';

@Component({
    selector: 'nav-menu',
    templateUrl: './navmenu.component.html',
    styleUrls: ['./navmenu.component.css']
})
export class NavMenuComponent {

    public selectedAccount: Account;
    public showAccountList: boolean;
    public apiEnumType = ApiType;

    constructor(private accountService: AccountService, private levelService : LevelService) {
        this.showAccountList = false;
        accountService.accountChanged$.subscribe(account => {
            this.selectedAccount = account;
            this.showAccountList = false;
        });
    }

    onLevelChanged(newLevel: Level) {
        if (!this.selectedAccount)
            return;
        this.levelService.onLevelChange(newLevel);
    }

}
