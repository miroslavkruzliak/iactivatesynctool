﻿import { Subscription } from 'rxjs/Subscription'
import { LoadingService } from '../services/shared/loading.service'

export class BaseComponent {

    constructor(private loadingService: LoadingService) {
    }

    protected set isLoading(subscription: Subscription) {
        this.loadingService.load(subscription);
    } 
}