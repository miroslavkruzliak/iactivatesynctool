﻿export enum AlertType {
    Success,
    Error,
    Info,
    Warning
}


export class Alert {

    constructor(public message: string, public type: AlertType) {
    }
}