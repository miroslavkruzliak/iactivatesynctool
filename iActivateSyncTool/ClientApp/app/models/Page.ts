﻿export class Page<T>
{
    public items: T[];
    public totalCount: number;
    public page: number;
    public pageTotal: number;
}