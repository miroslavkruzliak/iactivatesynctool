﻿export class Account {
    public id: number;
    public name: string;
    public apiType: ApiType;
}

export enum ApiType {
    AdWords = 1,
    Bing,
    YahooJapan
}