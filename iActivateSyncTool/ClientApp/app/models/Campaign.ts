﻿export class Campaign {
    public apiCampaign: ApiCampaign;
    public dbCampaign: DbCampaign;
    public differences: string[];
}

export class ApiCampaign {
    public id: number;
    public name: string;
}

export class DbCampaign {
    public id: number;
    public name: string;
}