﻿import { Account } from './Account';
import { Campaign } from './Campaign';

export class Filter {
    public account: Account;
    public campaigns: Campaign[];
}