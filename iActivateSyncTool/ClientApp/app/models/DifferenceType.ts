﻿export enum DifferenceType {
    Difference,
    DbMissing,
    ApiMissing
}