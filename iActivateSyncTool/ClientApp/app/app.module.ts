import { NgModule, ErrorHandler } from '@angular/core';

// modules
import { RouterModule, Route } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { NavMenuModule } from './modules/navmenu.module';
import { CampaignsModule } from './modules/campaigns.module';

// components
import { AppComponent } from './components/app/app.component';
import { LoadingComponent } from './components/app/loading.component';
import { AlertComponent } from './components/app/alert.component';

// services
import { AlertService } from './services/shared/alert.service';
import { LoadingService } from './services/shared/loading.service';

// error handling
import { CustomErrorHandler } from './CustomErrorHandler';
 
const routes: Route[] = [
    { path: '**', redirectTo: ' ' }
];

@NgModule({
    bootstrap: [ AppComponent ],
    declarations: [
        AppComponent,
        AlertComponent,
        LoadingComponent
    ],
    imports: [
        BrowserModule,
        HttpModule,
        MatProgressSpinnerModule,
        NavMenuModule,
        CampaignsModule,
        RouterModule.forRoot(routes)
    ],
    providers: [LoadingService, AlertService, { provide: ErrorHandler, useClass: CustomErrorHandler }]
})
export class AppModule {
}
