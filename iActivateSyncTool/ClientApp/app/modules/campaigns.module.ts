﻿import { NgModule } from '@angular/core';

// modules
import { RouterModule, Route } from '@angular/router';

// components
import { CampaignListComponent } from '../components/campaign/campaign-list.component'

// services
import { CampaignsService } from '../services/campaigns.service'

const routes: Route[] = [
    {
        path: ':accountId:/:apiType/Campaigns',
        redirectTo: ':accountId:/:apiType/Campaigns/Difference'
    },
    {
        path: ':accountId:/:apiType/Campaigns/:operation',
        component: CampaignListComponent
    }
];

@NgModule({
    declarations: [CampaignListComponent],
    imports: [RouterModule.forChild(routes)],
    providers: [CampaignsService],
    exports: [CampaignListComponent]
})
export class CampaignsModule {
}