﻿import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common'; // for async pipe
import { RouterModule } from '@angular/router';

import { AccountService } from '../services/account.service';
import { LevelService } from '../services/shared/level.service';

import { NavMenuComponent } from '../components/navmenu/navmenu.component';
import { AccountListComponent } from '../components/account/account-list.component';
import { AccountNaviComponent } from '../components/shared/account-navi.component';
import { SearchPipe } from '../pipes/search.pipe';

@NgModule({
    declarations: [NavMenuComponent, AccountListComponent, SearchPipe, AccountNaviComponent],
    providers: [
        AccountService,
        LevelService
    ],
    imports: [
        CommonModule,
        FormsModule,
        RouterModule
    ],
    exports: [NavMenuComponent]
})

export class NavMenuModule {

}