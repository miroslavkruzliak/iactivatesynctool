using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using iActivateSyncTool.Data.DbEntities;
using iActivateSyncTool.Services;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using System.Net;
using iActivateSyncTool.Logic.ApiServices;
using System.IO;
using Newtonsoft.Json;
using iActivateSyncTool.Logic.Comparers;
using iActivateSyncTool.Logic;
using iActivateSyncTool.Data.Interfaces;
using iActivateSyncTool.Data.ApiEntities;

namespace WebApplicationBasic
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc();
            services.AddDbContext<AdServiceContext>(options => options.UseSqlServer(Configuration.GetConnectionString("TestConnection")));
            services.AddScoped<IAccountsService, AccountsService>();
            services.AddScoped<ICampaignsService, CampaignsService>();

            services.AddScoped<IApiService>(serviceProvider =>
            {
                var context = serviceProvider.GetRequiredService<IHttpContextAccessor>().HttpContext;

                if (context.Request.Path.HasValue)
                {
                    var segments = context.Request.Path.Value.Split('/');
                    if (segments.Length > 1)
                    {
                        if (segments[segments.Length - 2] == "AdWords")
                            return new AdWordsService();
                        else if (segments[segments.Length - 2] == "Bing")
                            throw new NotImplementedException();
                    }
                }

                throw new ApplicationException("Cannot determine API type from the path!");
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseExceptionHandler(builder => {
                    builder.Run(async context =>
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        context.Response.ContentType = "application/json";

                        var ex = context.Features.Get<IExceptionHandlerFeature>();
                        if (ex != null)
                        {
                            var error = new
                            {
                                Error = ex.Error.InnerException?.Message ?? ex.Error.Message
                            };

                            using (var writer = new StreamWriter(context.Response.Body))
                            {
                                new JsonSerializer().Serialize(writer, error);
                                await writer.FlushAsync().ConfigureAwait(false);
                            }
                        }
                    });
                });
                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions {
                    HotModuleReplacement = true,
                    HotModuleReplacementEndpoint = "/dist/__webpack_hmr"
                });
                
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "app_route",
                    template: "{accountId}/{apiType}/{controller:regex(^(Campaigns|AdGroups|Ads)$)}/{action=Differences}");
                

                routes.MapRoute(
                    name: "accounts",
                    template: "Accounts",
                    defaults: new { controller = "Accounts", action = "List" });

                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action}",
                    constraints: new { controller = "Home", action = "Index" },
                    defaults: new { controller = "Home", action = "Index" });

                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new { controller = "Home", action = "Index" });
            });
        }
    }
}
