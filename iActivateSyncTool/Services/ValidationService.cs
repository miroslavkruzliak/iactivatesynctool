﻿using iActivateSyncTool.Data;
using iActivateSyncTool.Data.DbEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iActivateSyncTool.Services
{
    public class ValidationService
    {
        protected AdServiceContext db;
        public ValidationService(AdServiceContext db)
        {
            this.db = db;
        }

        protected async Task<ApiAccount> ReturnAccountIfValid(int accountId, ApiType apiType)
        {
            var account = await this.db.ApiAccount
                .Include("AdWordsAccount")
                .Include("BingAccount")
                .SingleOrDefaultAsync(apiAccount => apiAccount.Id == accountId);

            if (account == null)
                throw new ArgumentException($"Account ID '{accountId}' does not correspond with any account!");

            if (account.Api != (short)apiType)
                throw new ArgumentException($"API '{apiType}' does not correspond with account ID '{accountId}'!");

            return account;
        }
    }
}
