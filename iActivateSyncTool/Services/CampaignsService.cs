﻿using iActivateSyncTool.Data;
using iActivateSyncTool.Data.ApiEntities;
using iActivateSyncTool.Data.DbEntities;
using iActivateSyncTool.Data.Interfaces;
using iActivateSyncTool.Logic;
using iActivateSyncTool.Logic.ApiServices;
using iActivateSyncTool.Logic.Comparers;
using iActivateSyncTool.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iActivateSyncTool.Services
{
    public class CampaignsService : ValidationService, ICampaignsService
    {
        private IApiService apiService;
        private CampaignComparer campaignComparer;
        public CampaignsService(AdServiceContext db, 
            IApiService apiService) : base(db)
        {
            this.apiService = apiService;
            this.campaignComparer = new CampaignComparer();
        }

        public async Task<CampaignViewModel[]> GetCampaignDifferencesAsync(int accountId, ApiType apiType)
        {
            var (dbCampaigns, apiCampaigns) = await GetCampaignsAsync(accountId, apiType);
            
            return this.campaignComparer.GetDifferences(dbCampaigns, apiCampaigns)
                .Where(r => r.Differences.Length > 0)
                .Select(r => new CampaignViewModel { ApiCampaign = (ApiCampaign)r.ApiEntity,
                    DbCampaign = (IDbCampaign)r.DbEntity,
                    Differences = r.Differences
                })
                .ToArray();
        }

        public async Task<CampaignViewModel[]> GetMissingApiCampaignsAsync(int accountId, ApiType apiType)
        {
            var (dbCampaigns, apiCampaigns) = await this.GetCampaignsAsync(accountId, apiType);

            return this.campaignComparer
                .GetApiMissing(dbCampaigns, apiCampaigns)
                .Select(db => new CampaignViewModel
                {
                    ApiCampaign = null,
                    DbCampaign = (IDbCampaign)db,
                    Differences = null
                }).ToArray();
        }

        public async Task<CampaignViewModel[]> GetMissingDbCampaignsAsync(int accountId, ApiType apiType)
        {
            var (dbCampaigns, apiCampaigns) = await this.GetCampaignsAsync(accountId, apiType);

            return this.campaignComparer
                .GetDBMissing(dbCampaigns, apiCampaigns)
                .Select(api => new CampaignViewModel
                {
                    ApiCampaign = (ApiCampaign)api,
                    DbCampaign = null,
                    Differences = null
                }).ToArray();
        }

        private async Task<(IDbCampaign[], ApiCampaign[])> GetCampaignsAsync(int accountId, ApiType apiType)
        {
            var account = await ReturnAccountIfValid(accountId, apiType);

            ApiCampaign[] apiCampaigns;
            IDbCampaign[] dbCampaigns;
            switch (apiType)
            {
                case ApiType.AdWords:
                    apiCampaigns = await apiService.GetApiCampaignsAsync(account.AdWordsAccount.ClientCustomerId);
                    dbCampaigns = await this.db.AdWordsCampaign.Where(c => c.AccountId == accountId).ToArrayAsync();
                    break;
                case ApiType.BingAds:
                    apiCampaigns = await apiService.GetApiCampaignsAsync(account.BingAccount.CustomerId);
                    dbCampaigns = await this.db.BingCampaign.Where(c => c.AccountId == accountId).ToArrayAsync();
                    break;
                default:
                    throw new ApplicationException("Unknown API type!");
            }

            return (dbCampaigns, apiCampaigns);
        }
    }

    public interface ICampaignsService
    {
        Task<CampaignViewModel[]> GetCampaignDifferencesAsync(int accountId, ApiType apiType);
        Task<CampaignViewModel[]> GetMissingDbCampaignsAsync(int accountId, ApiType apiType);
        Task<CampaignViewModel[]> GetMissingApiCampaignsAsync(int accountId, ApiType apiType);
    }
}
