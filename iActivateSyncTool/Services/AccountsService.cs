﻿using iActivateSyncTool.Data;
using iActivateSyncTool.Data.DbEntities;
using iActivateSyncTool.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iActivateSyncTool.Services
{
    public class AccountsService : IAccountsService
    {
        private AdServiceContext db;
        public AccountsService(AdServiceContext db)
        {
            this.db = db;
        }

        public async Task<AccountViewModel[]> GetAccountsAsync()
        {
            var tmp =  await (from apiAccount in this.db.ApiAccount
                    select new AccountViewModel
                    {
                        Id = apiAccount.Id,
                        ApiType = (ApiType)apiAccount.Api,
                        Name = apiAccount.Name
                    }).ToArrayAsync();

            return tmp;
        }
    }

    public interface IAccountsService
    {
        Task<AccountViewModel[]> GetAccountsAsync();
    }
}
